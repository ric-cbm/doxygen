# Changelog - Doxygen

--------------------------------------------------------------------------------
## 2016-12-14
1.  Helper Scripts:
    - Adding support for 'trailing_page_break' to 'generate()' methods used in 
      creating markdown summaries.
    - Moving visible line divider [the '<hr>' tag] to a standalone component in 
      'Formatting{}' [from 'page_break' variable]. Change will allow 
      customization scripts to utilize page breaks without inserting divider 
      where not desired [in Doxygen HTML output].


--------------------------------------------------------------------------------
## 2016-12-12
1.  First addition of changelog. Lots of history not accounted for, but will do 
    best to indicate changes for this release alone.
2.  Migrated from system specific scripts [Batch && Bash] to general Python 
    scripts for both 'standalone' and 'relative' scripts.
3.  Ensured Python scripts can run on:
    - Windows 7 x64 for Python 2.7.X++ and 3.4.X++
    - Debian Jessie for Python 2.7.X++ and 3.4.X++
4.  Migrated <scripts/helper_scripts> to a nest of two packages:
    - base_files:
        - Contains base classes with raw structure and parsing abilities but no 
          file generation / direct interaction [structure there, but the user 
          has to indicate what to do].
    - implementations:
        - Extends the 'base_files' and adds specific file generation abilities.
          For example, a specific file targeted at generating custom command 
          summaries.
5.  Expanded the PDF generation ability via both <helper_scripts> and standlone 
    scripts such that:
    - Some of the 'Generated By ... ' and timestamp inforamtion is excluded.
    - The 'Overview.pdf' summary file is also split into two files to make 
      it easier to find what you are looking for:
        - 'Overview--Source-Code.pdf'
            - Source and file summaries.
        - 'Overview--Notes.pdf'
            - Contains all supplementary materials [not related to source code].
    - Auto generates a <docs/section_pdfs> directory for each chapter or 
      section in the base LaTeX file generated by Doxygen. This will alow 
      sharing of a single PDF chapter with a developer [more targeted sharing].
6.  Added support for the following custom supported commands [tags] to all 
    templates:
    - [at]reference_name
        - Use to provide a shortname for a file or a command.
    - [at]depends
        - Use to provide a bulleted list for 'dependencies'.
7.  Added version numbering support for Doxyfiles via a new 
    'Doxyfile--version.conf' file. This file can be modified by a 
    <customization> script to make it dynamic. Each template uses the [at]INCLUDE 
    tag to auto-include this file in the build.
8.  Updated all README files to help improve clarity.
9.  Ensured all folders that may be copied out of this project have a 
    '.version' file within them.
10. Updated/set version numbers for project components:
    1. <scripts/helper_scripts> @v1.0.0
    2. <scripts/relative>       @v2.0.0
    3. <scripts/standalone>     @v2.0.0
    4. <templates/cmd_tags>     @v1.1.0
    5. <templates/general>      @v1.4.0


