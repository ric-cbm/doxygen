# REPOSITORY OVERVIEW

This repository serves to:

  1. Provide installation materials and instructions for installing:  Doxygen, 
     Graphviz, and MiKTex.

  2. Provide scripts and structure for 'templates' to use to quickly generate 
     summaries of your source code.

For full details on how to integrate the materials provided in this repository 
with your project, please see:
  
  1. [<scripts>/README.md](scripts/README.md)
    
    - Covers what components are utilized by the available scripts and how to 
      integrate them with your project.
  
  2. [<templates>/README.md](templates/README.md)
    
    - Covers what each template does and how to use them as a launching point 
      for your Doxygen integration.




# A NOTE ON LICENSING

I did not make the items provided in the <installer_packages> 
directory. Rather, these are downloaded directly from the 'downloads' page
of the managing websites. All items included were available for free and
easy access on the public web-pages of the orinal developer sites.

The install materials are provided as a reference here in the event
the original website(s) go down or run into versioning issues. PLEASE SEE
"CREDITS.TXT" FOR FULL DETAILS ON HOW TO OBTAIN/REFERENCE THE DEVELOPER
SITES.

The "LICENSE.TXT" file applies to all files OUTSIDE of the  <install_pacakges> 
directory. For licdensing details on the installer packages, please see the 
developer website(s).




# INSTALLATION => WINDOWS

1. Install the following from the <install_packages\windows> directory:
    
    1. doxygen-<VER-#>-setup.exe
    
    2. basic-miktex-<VER-#>.exe
        
        > NOTE:  You will be prompted to install missing packages when first 
                 using the utility (i.e. running a pdf generator script for the 
                 first time).
        
    
    3. graphvix-<VER-#>.msi:
        
        - It is recommended to select 'for all users' when installing [defaults 
          to current user only].
        
        > NOTE:  Default installation settings are fine unless otherwise 
                 indicated.
        
    
2. Append the following to your system path to support Graphviz [required for 
   versions >= 2.3.1]. Without the edit, you cannot run / invoke Graphiz 
   utilities (such as 'dot') from the command line:
    
    - C:\Program Files (x86)\Graphviz2.38\bin




# INSTALLATION => DEBIAN LINUX

1. Open a command terminal and install the following utilities:
    
    1. Doxygen && doxygen-latex support:
        * sudo apt-get update
        * sudo apt-get install doxygen doxygen-latex doxygen-gui
    
    2. Graphviz:
        * sudo apt-get install graphviz




