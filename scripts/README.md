# Available Scripts

1. <helper_scripts>
    
    - Directory containing helper script packages used in some of the 
      standalone scripts as well made available to any 'customization' 
      script.

2. <relative>:
    
    - Directory containing update scripts for use with Windows and Linux.
    
    - All scripts are relative to the <standalone> scripts.
    
    - Intended to be used in larger projects when it makes more sense to have a 
      single location for managing core update scripts.

3. <standalone>:
    
    - Directory containing update scripts for use with Windows and Linux.
    
    - Core scripts that can be 'dropped into' a template folder and run.
    
    - Run either from within a Doxygen template folder or relatively.




## Using the Scripts in Your Project

The suggested workflow for using these scripts within your project is to:

  1. Create a <docs> folder [or preferred name] within your project [typically 
     within the root of the project].
  
  2. Create a Doxygen instance or use on of the available <templates> provided 
     in this repository.
  
  3. Apply all necessary changes and customizations to the Doxyfile.conf.
    
    - If using one of the provided <templates>, please see the README.md 
      overview within the <templates> folder for tips and suggestions on what 
      to customize.
  
  4. Copy over support for Doxygen scripts in one of two fashions:
    
    1. Direct integration:
        
        - Intended to allow projects to update documentation without any 
          external references [self-contained].

        - Suggested integration structure/process:
            
            1. Create a <.update_overview> directory within your documentation 
               folder.
                
            2. Copy the <standalone> folder to the <.update_scripts> 
               directory.
               
            3. Copy the <helper_scripts> folder to the <.update_scripts> 
               directory.
               
            3. [OPTIONAL] Add a <customizations> directory within the 
               <.update_scripts> directory.
      
    2. Relative integration:
        
        - Intended to allow a central location for managing all non-
          customization scripts. This should allow for less maintenance on large 
          scale projects composed of multiple repositories.
               
        - Suggested integration structure/process:
            
            - Copy the <scripts/relative> folder to the <.update_scripts> 
              directory.
            
            - Modify the paths referenced within all 'realtive' scripts.
            
            - [OPTIONAL] Add a <customizations> directory within the 
              <.update_scripts> directory.
            
> A couple of notes on the <scripts> integration above:
>   
>   1. While you can run scripts from anywhere, keeping them nested within your 
>      docs folder will allow them to automatically detect and locate the 
>      Doxyfile and relevant directories automatically in most cases.
>   
>   2. The <customizations> folder is not required, but any 'python' script 
>      held within it will be run whenever the 'run_customizations.py' standalone 
>      script is executed.




