####################################################################################################
# @brief        Provides methods to generate page summaries for Doxygen summary blocks that utilize
#               the '@file' tag.
# 
# @details      Expands upon the following base class(es):
#                   1. SummaryPage() => FileHeaderPage():
#                       * Replaces the 'generate_page()' method.
#                       * Replaces the '_caller_syntax()' method.
#               
#               Additionally, provides the following method(s):
#                   1. generate_file_page_summaries():
#                       - Wrapper to take a path to a directory and generate a 'file header page' 
#                           summary for all files found in the provided source directory.
# 
# @author       Michael Stephens (MCS)
# 
# @attention    DEPENDENCIES:
#                   * Python 2.7.X
####################################################################################################


####################################################################################################
## Import Modules
####################################################################################################
from __future__ import print_function                                           # Add support for update print function.
import os, sys                                                                  # Manipulate system-level functions.
import io                                                                       # Used to write raw bytes to file.
from ..base_files import markdown_summaries                                     # Import markdown summary library.
from ..base_files import parser                                                 # Used in standalone method to generate Doxygen summaries to then summarize as markdown files.



    
####################################################################################################
# @class        FileHeaderPage
# 
# @brief        Extends the 'SummaryPage()' class for the '@file' option used in the comment block 
#               at the top of a file used to indicate it should be processed by Doxygen.
#####################################################################################################
class FileHeaderPage(markdown_summaries.SummaryPage):
    
    
    ################################################################################################
    # @brief        Constructor for the class.
    # 
    # @param[in]    data                    List containing dictionary(ies) summarizing a Doxygen 
    #                                       project. The format should match that of 
    #                                       'SummarizeDoxgenComments.data'.
    # 
    # @returns      Passes through any returns from parent class.
    ################################################################################################
    def __init__(self, data):
        # Initialize parent class.
        return super(FileHeaderPage, self).__init__(data)
    
    
    ################################################################################################
    # @brief        Structures a string representing the reference name to use for the file.
    # 
    # @details      Parses the provided 'data entry' (from the global 'self.data[] list') and 
    #               generates a string containing the name to reference when referring to the file.
    # 
    # @param[in]    data_entry              Single entry from global 'self.data' (e.g. data[n]). The 
    #                                       values within this data set will be processed to create 
    #                                       a single-page summary of the data.
    # 
    # @retval       String                  Message string containing syntax required to invoke the 
    #                                       current command.
    ################################################################################################
    def _caller_syntax(self, data_entry):
        # Define local variable(s) used in function.
        syntax                              = ""                                # String containing formatted command [caller] syntax.
        
        # Start command syntax with name of command.
        if "cmd_name" in data_entry:
            syntax += "[%s" % data_entry["cmd_name"][0].strip()
        # >> Error Prevention. Exit if no target or command name in data set:
        else:
            return None
        
        # Add any 'required' command parameters.
        if "arg_param_req" in data_entry:
            for arg in data_entry["arg_param_req"]:
                if not "=" in syntax:
                    syntax += "=%s" % str(arg.split(" ")[0]).strip()
                else:
                    syntax += "|%s" % str(arg.split(" ")[0]).strip()
        
        # Add any 'optional' command parameters.
        if "arg_param_opt" in data_entry:
            for arg in data_entry["arg_param_opt"]:
                if not "=" in syntax:
                    syntax += "=%s*" % str(arg.split(" ")[0]).strip()
                else:
                    syntax += "|%s*" % str(arg.split(" ")[0]).strip()
        
        # Close command string and return to caller.
        return syntax + "]"
    
    
    ################################################################################################
    # @brief        Parses the provided 'data entry' and generates a page overview summary.
    # 
    # @details      Generates a summary after validating the 'data entry' is non-empty.
    # 
    # @param[in]    data_entry              Single entry from global 'self.data' (e.g. data[n]). The 
    #                                       values within this data set will be processed to create 
    #                                       a single-page summary of the data.
    # 
    # @warning      Opting ***NOT*** to invoke the parent class instance of function. Want to 
    #               completely replace original output structure generated by parent class.
    # 
    # @returns      String containing summary information for the 'data entry' provided.
    ################################################################################################
    def generate_page(self, data_entry):
        
        # Start out with reference name [if one provided]:
        if "reference_name" in data_entry:
            name        = str(data_entry["reference_name"][0]).strip()
            output = ( "## %s %s" % (name, self.paragraph_break) )
        else:
            output = ""
        
        # Add 'description' section as grouping of 'brief' and 'details'.
        tmp = []
        if "brief" in data_entry:
            tmp.extend(data_entry["brief"])
        if "details" in data_entry:
            tmp.extend(data_entry["details"])
        #### 
        if len(tmp) > 0:
            output += self._add_section("", tmp)
        
        # Add 'notes' section.
        if "note" in data_entry:
            output += self._add_section("Note(s)", data_entry["note"])
        
        # Add 'warning' section.
        if "warning" in data_entry:
            output += self._add_section("Warning(s)", data_entry["warning"])
        
        # Add 'code' section.
        if "code" in data_entry:
            output += self._add_section("Sample Code", data_entry["code"], treat_as_code=True)
        
        # Add 'dependencies' section.
        if "depends" in data_entry:
            output += self._add_section("Dependencies", data_entry["depends"])
        
        # Add 'version' section.
        if "version" in data_entry:
            output += self._add_section("Version Info", data_entry["version"])
        
        # Return result
        return output




####################################################################################################
# @brief            Function used to generate all summary markdown files at a given path. Serves as 
#                   wrapper to allow utilization of the above classes in a <customization> script 
#                   in Doxygen.
# 
# @param[in]        path_source             Path to root of directory to parse.
# @param[in]        path_summaries          Path to root of summary folder to store results in.
# 
# @retval           files_refs              List of references to markdown files generated.
####################################################################################################
def generate_file_page_summaries(path_source, path_summaries):
    # :: CHECK :: Ensure path is valid before proceeding. ::
    if not os.path.exists(path_source):
        return None
    
    # :: CHECK :: Ensure output directories exist before processing any files. ::
    if not os.path.exists(path_summaries):
        os.makedirs(path_summaries)
    
    # Begin the process of summarizing a Doxygen file header by instantiating an instance of the  
    # summary class.
    sdc = parser.SummarizeDoxgenComments( path_source )
    
    # Loop through all files in the provided path and generate table and page summaries.
    files_refs = []
    for root, dirs, files in os.walk( path_source ):
        for f in files:
            
            # Determine file information and skip file if it does not end in ".py".
            fname, fexten   = os.path.splitext(f)
            fpath           = os.path.join( path_summaries, (str(fname) + ".md") )
            if fexten != ".py":
                continue
            
            # Parse the latest file and notify the user of how many profile types were located.
            data    = sdc.parse_file(f, sort_by_func_name=False, required_tags={"file": ['*']}, preserve_bp_indentation=True)
            
            # :: CHECK :: Ensure at least one profile type found. ::
            if data is None or len(data) is 0:
                print("  > File header not marked for inclusion. Skipping '%s'." % f)
                continue
            print("  > Located '%d' profile types." % len(data))
            
            # Generate a 'file header summary page' for the file parsed.
            print("  > Saving summary page at:  '%s'." % (fpath))
            csp     = FileHeaderPage(data)
            if sys.version[0] >= "3":
                fptr    = io.open(fpath,  'w',    newline='')
            else:
                fptr    = open(fpath,     'wb')
            fptr.write( csp.generate( "# " + fname.replace("_", " ").title() + (" {#file_header_summary_%s}" % fname), trailing_page_break=True ))
            fptr.close()
            
            # Record all file references generated.
            files_refs.append( ("file_header_summary_%s" % fname) )
            
    
    # Return list containing all files found to contain 
    return files_refs




