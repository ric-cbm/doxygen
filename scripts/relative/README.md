# AVAILABLE SCRIPTS

1. 'launch_standalone_script.py':
    
    - Main script used to launch a standalone script. This script is imported 
      and its 'launcher()' method is invoked with the path to the desired 
      'standalone' directory to use by all other scripts in this directory.

2. 'run_customization_scripts.py':
    
    - Wrapper to the main "run_customization_scripts.py" script located in the 
      <doxygen.git/scripts/standalone> directory.

3. 'update_all.py':
    
    - Wrapper to the main "update_all.py" script located in the 
        <doxygen.git/scripts/standalone> directory.

4. 'update_doxygen.py':
    
    - Wrapper to the main "update_doxygen.py" script located in the 
        <doxygen.git/scripts/standalone> directory.

5. 'update_summary_pdf.py':
    
    - Wrapper to the main "update_summary_pdf.py" script located in the 
        <doxygen.git/scripts/standalone> directory.


NOTE:  Make sure to replace ** <RELATIVE_OR_ABSOLUTE_PATH_HERE> ** with 
       your desired relevant or abolute path to an instance of the 'standalone' 
       scripts.




