####################################################################################################
# @file
# 
# @brief            Looks for any available customization scripts in the requested directory and 
#                   attempts to execute each one.
# 
# @details          Locates the provided <customizations> directory and attempts to process any 
#                   file which meets the following criteria:
#                       1. Is a Python script.
#                       2. Is NOT:
#                           - An '__init__.py' script.
#                           - A file ending in '.pyc'.
# 
# @warning          If no path is provided, the script will search upwards for a directory named 
#                   <customizations> starting at the location of this script. A max of '3' levels 
#                   will be checked and the first instance found will be used. If no instance is 
#                   found, the script will exit with a silent error [error code not set as 
#                   customizations not required].
# 
# @param[in]        path                    Path to directory to begin parsing.
# @param[in]        flags                   Any of the supported flags:
#                                               - -s | --silent
# 
# @attention        DEPENDENCIES:
#                       - Python {2.7.9++; 3.4.2++}
#                       - [Any dependencies required for scripts in the <customization> folder].
# 
# @note             This function does not consider failing to find a valid <customization> folder 
#                   as an error [returns success as customizations not required].
# 
# @retval           0                       Successful completion of script.
# @retval           1                       Error code indicating one of the customization scripts 
#                                           invoked returned an error.
####################################################################################################


####################################################################################################
# IMPORT MODULES
####################################################################################################
import sys, os                                                                  # General system level access/commands.
import argparse                                                                 # Adds support for processing command line arguments.

# Ensure 'press any key' will work for Python 3 instances.
if sys.version[0] >= "3":
    raw_input=input




####################################################################################################
# DEFINE COMMAND LINE ARGUMENTS
####################################################################################################
# Create parser instance with description.
# >> NOTE:  The optional 'allow_abbrev=False' to exclude partial matches is not available in the 
#           present version of Python 2.7.X accessible on my development machine. Consider for 
#           future changes.
parser = argparse.ArgumentParser(   description="Attempts to run all Python scripts within the \
                                                 directory at the provided path.")

# Add options to parser with '-h|--help' support.
parser.add_argument('path',                         nargs="?",                          help="Path to <customizations> directory to process.")
parser.add_argument('-s', '--silent',                           action='store_true',    help="Flag indicating that 'silent mode' was requested. Silent mode ensures there is no pause after the script finishes executing.")
parser.add_argument('-d', '--directory',            nargs=1,                            help="[OPTIONAL] Path to directory to use when searching for parameters if the provided items can not be found.")




####################################################################################################
# @brief            Wrapper function to allow passing parameters when invoking the main processing 
#                   loop from a parent script [e.g. 'update_all.py'].
# 
# @note             This function does not consider failing to find a valid <customization> folder 
#                   as an error [returns success as customizations not required].
# 
# @retval           0                       Successful completion of function.
# @retval           1                       Error code indicating one of the customization scripts 
#                                           invoked returned an error.
####################################################################################################
def main(arg_list=None):
    # Grab access to global variables related to script parameters.
    global parser, args
    
    # Process all arguments provided to script.
    args = parser.parse_args(arg_list)
    
    # :: CHECK :: Validate path provided. ::
    # >> Path not provided or is invalid. Override with default path value:
    if not 'path' in vars(args) or args.path is None or not os.path.isdir(args.path):
        # Notify user of override.
        print("[WARNING]> The 'Run Customization Scripts' script requires the path to the 'customization' directory as a parameter to the script.")
        print("Enforcing use of default path value.")
        
        # Determine base directory to start searching upwards from.
        if args.directory != None:
            base            = os.path.realpath(args.directory[0])
        else:
            base            = os.path.dirname(os.path.realpath(__file__))
        
        # Look for <customizations> folder in the current working directory and 'n-levels' above:
        flag_valid_path = False
        levels          = os.path.join("..", "..")
        lvl_cnt         = 0
        while lvl_cnt != 4:
            path = os.path.join( base, levels[0:3]*lvl_cnt, "customizations" )
            if os.path.isdir(path):
                flag_valid_path = True
                break
            else:
                lvl_cnt += 1
        
        # Determine absolute path or exit with NO ERROR if no valid path determined.
        if flag_valid_path == True:
            path = os.path.realpath(path)
        else:
            print("No customizations directory located. Skipping.")
            return 0
           
    # >> Path is okay. Store absolute path in global variable:
    else:
        path = os.path.realpath(args.path)
    
    
    ################################################################################################
    # INVOKE ALL RELEVANT CUSTOMIZATION SCRIPTS.
    ################################################################################################
    # Notify user of processing step.
    print( "Will attempt to process any Python scripts in the <%s> directory." % str(path) )
    
    # Loop through customization scripts and invoke each one.
    rc = 0
    pwd = os.getcwd()
    for root, dirs, files in os.walk(path, topdown=True):
        
        # Change to directory containing file.
        os.chdir(root)
        
        # Append current path to system path if not already a component.
        if not os.path.realpath(root) in sys.path:
            sys.path.append(root)
        
        # Process all files in current directory.
        for f in files:
            # Breakout file information.
            fname, fexten = os.path.splitext(f)
            
            # :: CHECK :: Ensure file [type] is supported. ::
            if fexten != ".py" or fname == "__init__" or fexten == ".pyc": 
                continue
            
            # Process file.
            # >> NOTE:  Warn, don't bail on error(s).
            print("Processing '%s' ..." % str(f))
            try:
                __import__(fname)
            except:
                # >> Make sure not to prematurely mark a file as exiting with error is simple 
                #    'sys.exit(0)' was called to abort script without an error condition.
                if str(sys.exc_info()[1]) != '0':
                    rc = 1
                    print("  [WARNING]> Customization script exited with error. Preses any key to continue ...")
                    raw_input()
    
    # Return to original working directory and exit without present value for rc.
    os.chdir(pwd)
    return rc




####################################################################################################
# TREAT SCRIPT AS STANDALONE IF FILE WAS NOT IMPORTED.
####################################################################################################
if __name__ == "__main__":
    # Invoke main function with all but the name of the current script as a parameter list for the 
    # argparse module.
    rc = main(sys.argv[1:])
    
    # Pause execution so long as 'silent' mode not set [flag not passed to function].
    if args.silent == False:
        raw_input("Press any key to continue ...")
    
    # Exit with exit code from main function.
    sys.exit(rc)




