# TEMPLATES OVERVIEW

All templates listed here are meant to be copied and pasted into the root 
directory for your project.

All templates provide:
    
  1. <html>:
    
    - The directory where Doxygen [web] summaries are generated in.
    
    - Provided as a placeholder to help prevent errors.
  
  2. '.version':
    
    - Summary file containing name and version info.
  
  3. 'Doxyfile.conf':
    
    - All settings used by Doxygen when generating a summary are defined within 
      this file.
  
  4. 'Doxyfile--version.conf':
      
    - Placeholder file that should minimally contain the version of the project/
      Doxygen version being compiled. Ideally, should be updated automatically 
      by a <customization> script.
  
  5. 'Overview.html':
      
    - File is a shortcut [web redirect] to the summary page within the html 
      folder.

> Additionally, while not provided, all templates are intended to be used with 
  a copy of the <scripts> directory. For specifics on how to integrate the 
  <scripts> directory, please see the associated README file.
    



# MODIFYING TEMPLATES

The names of the templates are not important and can/should be changed to fit 
your needs. Suggested name change is to 'docs'.

While the template names are flexible, the file names for contents within them 
are NOT.

** Please do not forget to validate the path and file name information within 
each script the <.update_overview> directory (e.g. change Doxyfile name; 
replace 'REALTIVE_PATH_TO_SCRIPTS_DIRECTORY' with your relevant path; etc.). **




# SUGGESTED CUSTOMIZATIONS OF 'Doxyfile.conf'

1. Make sure to edit the 'PROJECT_NAME'. This will be visible in much of the 
   generated material.
    
    > HINT:  Search for "PROJECT_NAME           =".

2. The defeault template presumes it is placed in the root of the template and 
   that all folders external to 'itself' should be considered. To edit the list 
   of directories it should NOT parse, please edit 'INPUT'.
    
    > HINT:  Search for "INPUT                  =".
    
3. The types of supported files (by extension type) parsed by Doxygen is 
   defined by "FILE_PATTERNS".
    
    > HINT:  Search for "FILE_PATTERNS          =".
    
4. The list of directories NOT included in the parsing by Doxygen is defined by 
   "EXCLUDE".
    
    > HINT:  Search for "EXCLUDE                =".

5. By Default, if there is a markdown file named "README.md" in the directory 
   above the 'template' install location for your project, that file will be 
   automatically included in the 'index.html' page. If you wish to change this
   behavior [or location], please edit 'USE_MDFILE_AS_MAINPAGE'.
   
   > HINT:  Search for "USE_MDFILE_AS_MAINPAGE ="




# CURRENT AVAILABLE TEMPLATES

1. <cmd_tags>:
    
    - General template + support for custom 'cmd_tags'. Useful for API 
      documentation.

2. <general>:
    
    - Provides a good, generic base template for projects [a branching point].




# OTHER COMMENTS && SUGGESTIONS

1. It is recommended you do NOT commit the <latex> directory (not needed for 
   typical use cases). If using GIT, consider adding 'latex' to the '.gitignore' 
   file.




